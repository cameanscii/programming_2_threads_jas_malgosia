package pl.sda.jas_malgosia;

import java.util.concurrent.CountDownLatch;

public class JasMalgosiaThreads {


    public static void main(String[] args) throws InterruptedException {

        CountDownLatch latch=new CountDownLatch(2);


        Jas jas = new Jas(latch);
        Thread jasThread = new Thread(jas);
        jasThread.start();

        Malgosia malgosia = new Malgosia(latch);
        Thread malgosiaThread = new Thread(malgosia);
        malgosiaThread.start();


//        System.out.println("Main czeka na Jasia");
//        jasThread.join();
//        System.out.println("Maina czeka na Jasia");
//        System.out.println("Main czeka na Malgosie");
//        malgosiaThread.join();
//        System.out.println("Main skonczyl czekac na Malgosie");


        latch.await();
        System.out.println("Oboje są gotowi mogą iść na impreze");

    }

}

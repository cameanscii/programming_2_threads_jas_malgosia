package pl.sda.jas_malgosia;

import java.util.concurrent.CountDownLatch;

public class Malgosia implements Runnable{
        CountDownLatch latch;

    public Malgosia(CountDownLatch latch) {
        this.latch = latch;
    }

    public void run() {
            try {


                System.out.println("Malgosia idzie biegac");
                Thread.sleep(6000);
                System.out.println("Malgosia skonczyla bieganie");
                System.out.println("Malgosia bierze prysznic");
                Thread.sleep(2000);
                System.out.println("Malgosia jest juz czysta");
                System.out.println("Malgosia je sniadanie");
                Thread.sleep(1000);
                System.out.println("Malgosia najadla sie");
                System.out.println("Malgosia ubiera sukienke");
                Thread.sleep(1000);
                System.out.println("Malgosia jest ubrana");
                System.out.println("Malgosia idzie na spotkanie z kolezanka na kawe");
                Thread.sleep(25000);
                System.out.println("Malgosia wrocila z plotek");

                System.out.println("To koniec zadań na dziś dla Malgosi, odhacza się na liście");


                latch.countDown();

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


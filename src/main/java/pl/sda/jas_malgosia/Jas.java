package pl.sda.jas_malgosia;

import java.util.concurrent.CountDownLatch;

public class Jas implements Runnable {
    CountDownLatch latch;

    public Jas(CountDownLatch latch) {
        this.latch = latch;
    }

    public void run() {
        try {

            //pokazuje ile ma rdzeni
            System.out.println(Runtime.getRuntime().availableProcessors());
            System.out.println("Jaś przygotowuje i je śniadanie");
            Thread.sleep(5000);
            System.out.println("Jaś skończył śniadanie");
            System.out.println("Jaś bierze prysznic");
            Thread.sleep(3000);
            System.out.println("Jaś skończył brać prysznic");
            System.out.println("Jaś ubiera się");
            Thread.sleep(1000);
            System.out.println("Jaś już się ubrał");
            System.out.println("Jaś idzie do biedronki");
            Thread.sleep(15000);
            System.out.println("Jaś wraca z pełnymi siatkami, była dobra promocja w Biedronce");
            System.out.println("Jaś gra sobie na konsoli w Fife");
            Thread.sleep(5000);
            System.out.println("Jaś nagrał się aż oczy bolą");

            System.out.println("To koniec zadań na dziś dla Jasia, odhacza się na liście");
            latch.countDown();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
